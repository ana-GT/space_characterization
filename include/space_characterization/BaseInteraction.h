#pragma once

#include <Eigen/Core>

enum INTERACTION_TYPES {
  BASE_INTERACTION = 0,
  PICK = 1,
  PLACE = 2,
  NUM_INTERACTIONS = 3
};

/**
 * @class BaseInteraction
 */
class BaseInteraction {

  //friend class CLASS;

 public:
  BaseInteraction();
  virtual ~BaseInteraction();

  // ENLACE  14.4
 protected:
  
};
