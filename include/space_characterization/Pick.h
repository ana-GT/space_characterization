#pragma once

#include <Eigen/Core>
#include <sensor_msgs/JointState.h>
#include <geometry_msgs/Pose.h>
#include <robot_interaction_tools_msgs/CollisionObjectInfo.h>

/**
 * @class Pick.h
 * @brief Basic information to calculate a pick action
 */
class Pick {

 public:
  Pick();
  void set_arm_start_configuration( const sensor_msgs::JointState &_qs );
  void set_object( const robot_interaction_tools_msgs::CollisionObjectInfo &_object );
  void set_Tobj( const geometry_msgs::Pose &_Tobj );
  
 protected:
  sensor_msgs::JointState qs_;
  robot_interaction_tools_msgs::CollisionObjectInfo object_;
  geometry_msgs::Pose Tobj_;
  
};
