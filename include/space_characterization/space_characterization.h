#pragma once
/**
 * @file space_characterization.h
 */
#include <ros/ros.h>
#include <Eigen/Geometry>
#include <rit_utils/rdf_model_base.h>
#include <trac_ik_trajectories/cartesian_trajectories.h>
#include <kdl/chainjnttojacsolver.hpp>
#include <tf/transform_listener.h>
#include <tf_conversions/tf_kdl.h>
#include <tf_conversions/tf_eigen.h>

#include <cmath>
//#include <mlpack/methods/neighbor_search/neighbor_search.hpp>
#include <mlpack/methods/kmeans/kmeans.hpp>
#include <fstream>

#include <space_characterization/SeeJointLimits.h>

bool pInv( const Eigen::MatrixXd &_J,
	   Eigen::MatrixXd &_Ji,
	   double _eps = std::numeric_limits<double>::epsilon() );


/*****/
struct Sample_t {
  Eigen::VectorXd q;
  Eigen::Vector3d p;
  size_t cluster;

};

  // voxel_p << operator
  inline std::ostream &operator<<( std::ostream &_out,
			           const Sample_t& _vst ) {
    return _out << _vst.q.transpose() << " " << _vst.p.transpose() << " " << _vst.cluster<< std::endl;  
  }


double fRand( const double &_val_min,
	      const double &_val_max ) {
  return _val_min + (rand() /(double)RAND_MAX)*(_val_max - _val_min);
}

/** voxel_space_t */
struct voxel_space_t {
  
  std::string group_name;
  std::string grasp_link;
  std::string base_link;
  KDL::Frame Tf_ee_grasp, Tf_grasp_ee;

  int num_dofs;

  std::shared_ptr<TRAC_IK::JointGroup> joint_group;
  std::shared_ptr<KDL::ChainJntToJacSolver> jac_solver;
  std::shared_ptr<KDL::ChainFkSolverPos_recursive> fk_solver;
  KDL::Chain chain;
  KDL::JntArray lb, ub;
  
  bool init_voxel_space( std::shared_ptr<rit_utils::RDFModelBase> &_model,
			 std::string _group_name,
			 std::string _grasp_link ) {

    group_name = _group_name;
    grasp_link = _grasp_link;

    // Setup planners
    if( !_model->getJointGroup( group_name,
				joint_group ) ) {
    ROS_ERROR("Not being able to retrieve group %s for robot %s",
	      group_name.c_str(),
	      _model->getName().c_str() );
    return false;
    }

    base_link = joint_group->base_link;
    num_dofs = joint_group->joints.size();
    ROS_INFO("* Loaded robot %s limb with %d dofs and base link: %s",
	     _model->getName().c_str(), num_dofs,
	     base_link.c_str() );
  

    joint_group->chain_solver->getKDLLimits( lb, ub );
  
    //    KDL::Chain chain;
    joint_group->chain_solver->getKDLChain( chain );
    jac_solver.reset(new KDL::ChainJntToJacSolver(chain));
    fk_solver.reset( new KDL::ChainFkSolverPos_recursive(chain ) );

    return true;
  }

  /***/
  void update_tf_ee_grasp( tf::Transform _tf_ee_grasp ) {
    tf::transformTFToKDL(_tf_ee_grasp, Tf_ee_grasp );
    Tf_grasp_ee = Tf_ee_grasp.Inverse();
    joint_group->chain_solver->UpdateOffset( Tf_ee_grasp );
  }

  /****/
  KDL::JntArray generate_random_config( double _factor ) {

    // Get limits
    KDL::JntArray q( num_dofs );
  
    for( int i = 0; i < num_dofs; ++i ) {
      double range = (ub(i) - lb(i));
      double center = lb(i) + range*0.5;
      q(i) = fRand( center - 0.5*range*_factor, center + 0.5*range*_factor );
    }
    return q;
  }

  /***/
  void getJacs( KDL::JntArray _q,
		Eigen::MatrixXd &_Jlin,
		Eigen::MatrixXd &_Jang,
		Eigen::MatrixXd &_Jlin_inv,
		Eigen::MatrixXd &_Jang_inv ) {

    KDL::Jacobian jac( num_dofs );
    jac_solver->JntToJac( _q, jac );
    Eigen::MatrixXd Jac; Jac = jac.data;
    
    _Jlin = Jac.block(0, 0, 3, num_dofs );  
    _Jang = Jac.block(3, 0, 3, num_dofs );
    
    pInv( _Jlin, _Jlin_inv );
    pInv( _Jang, _Jang_inv );
    
  }

  
};

/**
 * @class SpaceCharacterization
 */
class SpaceCharacterization {

 public:
  SpaceCharacterization( ros::NodeHandle &_nh );
  bool init( std::string _group_name = "arm",
	     std::string _grasp_link = "tracarm/hand_fingertips" );
  void spin();
  void generate_samples( int _samples = 50000 );
  void generate_clusters( size_t _clusters );
  bool getTf( std::string _source,
	      std::string _target,
	      tf::Transform &_Tf );
  bool store();

  bool see_joint_limits( space_characterization::SeeJointLimits::Request &_req,
			 space_characterization::SeeJointLimits::Response &_res );
  
  // Debug
  void joint_state_cb( const sensor_msgs::JointState::ConstPtr &_msg );
  
 protected:
  ros::NodeHandle nh_;
  std::shared_ptr<rit_utils::RDFModelBase> model_;
  tf::TransformListener listener_;

  std::vector<Sample_t> samples_;
  voxel_space_t limb_; 
  size_t clusters_;

  ros::Subscriber sub_joint_state_;
  ros::Publisher pub_ellipsoid_marker_;
  ros::ServiceServer service_test_;
  sensor_msgs::JointState joint_state_;
};
