#pragma once
/**
 * @file Planner_Node.h
 * @brief Defines class that controls the input to planning problem (IM, IS)
 */

#include <ros/ros.h>
#include <interactive_markers/interactive_marker_server.h>
#include <robot_interaction_tools_msgs/CollisionObjectInfo.h>
#include <rit_utils/rdf_model.h>
#include <trac_ik_trajectories/cartesian_trajectories.h>
#include <interactive_markers/menu_handler.h>

/**
 * @class Planner_Node
 */
class Planner_Node {
  
 public:
  Planner_Node( ros::NodeHandle _nh );
  bool set_robot_params();
  
  visualization_msgs::InteractiveMarker create_3D_marker( const robot_interaction_tools_msgs::CollisionObjectInfo &_msg );
  void add_interactive_marker( visualization_msgs::InteractiveMarker _im );

  void process_feedback( const visualization_msgs::InteractiveMarkerFeedbackConstPtr &_feedback );
  void process_feedback_( const visualization_msgs::InteractiveMarkerFeedbackConstPtr &_feedback );

  
 protected:

  ros::NodeHandle nh_;

  interactive_markers::InteractiveMarkerServer server_;
  std::shared_ptr<rit_utils::RDFModel> rdf_model_;
  
  // Specific manipulator
  std::string group_;
  std::shared_ptr<TRAC_IK::JointGroup> joint_group_;
  std::shared_ptr<TRAC_IK::CartesianTrajectories> solver_;
  interactive_markers::MenuHandler menu_handler_;
};

