#pragma once

#include <ros/ros.h>
#include <space_characterization/Planner_Node.h>
#include <space_characterization/Pick_Planner.h>

/**
 * @class Pick_Planner_Node
 */
class Pick_Planner_Node : public Planner_Node {

 public:
  
  Pick_Planner_Node( ros::NodeHandle &_nh );
  void set_plan_config();
  void plan_pick( const visualization_msgs::InteractiveMarkerFeedbackConstPtr &_feedback );
  void execute_pick( const visualization_msgs::InteractiveMarkerFeedbackConstPtr &_feedback );
 protected:

  robot_interaction_tools_msgs::CollisionObjectInfo object_;
  std::shared_ptr<Pick_Planner> pick_planner_;
  Pick pick_;
};
