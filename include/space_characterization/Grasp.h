#pragma once
/**
 * @file Grasp.h
 */
#include <Eigen/Core>

/**
 * @class Grasp
 */
class Grasp {

 public:
  enum {
    SPACE_GRASP = 0,
    SQ_GRASP = 1,
    NUM_GRASP_TYPES = 2
  };
  
  Grasp();

 protected:
  
};
