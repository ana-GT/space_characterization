/***
 * @file Pick_Planner.h
 */
#pragma once

#include <trac_ik_trajectories/cartesian_trajectories.h>
#include <space_characterization/Grasp.h>
#include <space_characterization/Pick.h>

/**
 * @class Pick_Planner
 */
class Pick_Planner {

 public:

  Pick_Planner( ros::NodeHandle _nh,
		std::shared_ptr<TRAC_IK::JointGroup> _joint_group,
		std::shared_ptr<TRAC_IK::CartesianTrajectories> _solver,
		std::string _robot_name );

  bool setConditions( Pick &_pick );
  int calculateGrasps( Pick &_pick,
		       Eigen::VectorXd _data = Eigen::VectorXd(),
		       int _grasp_prim_type = Grasp::SPACE_GRASP,
		       double _pregrasp_dist = 0.1 );

  bool calculate_path( Pick &_pick,
		       double _app_dist = 0.1,
		       double _up_dist = 0.1 );
  void execute_path();

 protected:
  std::shared_ptr<TRAC_IK::CartesianTrajectories> solver_;
  std::shared_ptr<TRAC_IK::JointGroup> joint_group_;
  std::vector<sensor_msgs::JointState> path_;
  bool plan_available_;

  // HACK
  ros::Publisher traj_pub_;
  ros::NodeHandle nh_;
  
 public:
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW /**< For fixed-sized members **/
    
};
