cmake_minimum_required(VERSION 2.8.3)
project(space_characterization)

## Compile as C++11, supported in ROS Kinetic and newer
add_compile_options(-std=c++11)

## Find catkin macros and libraries
## if COMPONENTS list like find_package(catkin REQUIRED COMPONENTS xyz)
## is used, also find other catkin packages
find_package(catkin REQUIRED COMPONENTS
  planner_interface
  rit_utils
  robot_interaction_tools_msgs
  roscpp
  trac_ik_lib
  trac_ik_trajectories
  kdl_conversions
  tf_conversions
  interactive_markers
  )

find_package( PkgConfig )
set( ENV{PKG_CONFIG_PATH} "/home/ana/polycon/src/space_characterization/pkgconfig" )
pkg_search_module( MLPACK REQUIRED mlpack )
include_directories( ${MLPACK_INCLUDE_DIRS} )
link_directories(  ${MLPACK_LIBRARY_DIRS} )


add_service_files(
   FILES
   SeeJointLimits.srv
 )

## Generate added messages and services with any dependencies listed here
generate_messages(
   DEPENDENCIES
   robot_interaction_tools_msgs
 )


###################################
## catkin specific configuration ##
###################################
catkin_package(
  INCLUDE_DIRS include
  LIBRARIES space_characterization
  CATKIN_DEPENDS planner_interface rit_utils robot_interaction_tools_msgs roscpp trac_ik_lib trac_ik_trajectories kdl_conversions tf_conversions interactive_markers
#  DEPENDS system_lib
)

###########
## Build ##
###########

## Specify additional locations of header files
## Your package locations should be listed before other locations
include_directories(
 include
 ${catkin_INCLUDE_DIRS}
)

#add_library( space_characterization  src/space_characterization.cpp )
#target_link_libraries( space_characterization mlpack ${catkin_LIBRARIES} )

#add_executable( space_characterization_node  src/space_characterization_node.cpp )
#target_link_libraries( space_characterization_node space_characterization ${catkin_LIBRARIES} )

add_executable( test_attach_deattach_polycon  src/test_attach_deattach_polycon.cpp )
target_link_libraries( test_attach_deattach_polycon ${catkin_LIBRARIES} )

add_executable( test_attach_deattach_r2  src/test_attach_deattach_r2.cpp )
target_link_libraries( test_attach_deattach_r2 ${catkin_LIBRARIES} )

add_executable( test_attach_deattach_tracbot  src/test_attach_deattach_tracbot.cpp )
target_link_libraries( test_attach_deattach_tracbot ${catkin_LIBRARIES} )

add_executable( tracbot_crates  src/tracbot_crates.cpp )
target_link_libraries( tracbot_crates ${catkin_LIBRARIES} )

add_executable( attach_photoneo  src/attach_photoneo.cpp )
target_link_libraries( attach_photoneo ${catkin_LIBRARIES} )

add_executable( deattach_photoneo  src/deattach_photoneo.cpp )
target_link_libraries( deattach_photoneo ${catkin_LIBRARIES} )

add_library( pick_planner src/Pick_Planner.cpp src/Pick.cpp src/Grasp.cpp src/Planner_Node.cpp )
target_link_libraries( pick_planner ${catkin_LIBRARIES} )

add_executable( pick_planner_node src/pick_planner_node.cpp )
target_link_libraries( pick_planner_node pick_planner ${catkin_LIBRARIES} )

# add_dependencies(${PROJECT_NAME} ${${PROJECT_NAME}_EXPORTED_TARGETS} ${catkin_EXPORTED_TARGETS})
# set_target_properties(${PROJECT_NAME}_node PROPERTIES OUTPUT_NAME node PREFIX "")


