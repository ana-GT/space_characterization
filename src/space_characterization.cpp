/**
 * @file space_characterization.cpp
 */
#include <space_characterization/space_characterization.h>
#include <ros/package.h>
#include <Eigen/Eigenvalues>

/****/
bool pInv( const Eigen::MatrixXd &_J,
	   Eigen::MatrixXd &_Ji,
	   double _eps ) {

  
  if( _J.rows() < _J.cols() ) {
    bool b; Eigen::MatrixXd Jit;
    b = pInv( _J.transpose(),
	      Jit, _eps );
    _Ji = Jit.transpose(); 
    return b;
  }

  Eigen::JacobiSVD<Eigen::MatrixXd> svd = _J.jacobiSvd( Eigen::ComputeThinU | Eigen::ComputeThinV );
  Eigen::MatrixXd::Scalar tolerance = _eps*std::max( _J.cols(), _J.rows() )*svd.singularValues().array().abs().maxCoeff();

  _Ji = svd.matrixV()* Eigen::MatrixXd( Eigen::MatrixXd( (svd.singularValues().array().abs() > tolerance ).select( svd.singularValues().array().inverse(), 0) ).asDiagonal() )*svd.matrixU().adjoint();
}



/**
 * @function SpaceCharacterization
 */
SpaceCharacterization::SpaceCharacterization( ros::NodeHandle &_nh ) :
  nh_(_nh) {

}

/**
 * @function init
 */
bool SpaceCharacterization::init( std::string _group_name,
				  std::string _grasp_link ) {

  // Load rdfBaseModel
  model_.reset( new rit_utils::RDFModelBase() );

  std::string robot_description("/robot_description");
  std::string robot_description_semantic("/robot_description_semantic");
  if( !model_->init( robot_description_semantic,
		     robot_description ) ) {
    ROS_ERROR(" Error in loading model %s - %s ",
	      robot_description.c_str(),
	      robot_description_semantic.c_str() );
    return false;
  }

  // Load joint
  limb_.init_voxel_space( model_,
			  _group_name,
			  _grasp_link );

  // Get Tf from ee to grasp  
  tf::Transform tf_ee_grasp;
  //Tf_namespace_ + 
  getTf( limb_.joint_group->links.back(),
	 limb_.grasp_link,
	 tf_ee_grasp );
  limb_.update_tf_ee_grasp( tf_ee_grasp );

  // For testing
  sub_joint_state_ = nh_.subscribe("/joint_states", 1,
				   &SpaceCharacterization::joint_state_cb, this );
  
  pub_ellipsoid_marker_ = nh_.advertise<visualization_msgs::Marker>("task_ellipsoid", 1 );

  // Service
  service_test_ = nh_.advertiseService("see_joint_limits",
				       &SpaceCharacterization::see_joint_limits,
				       this );
  
  return true;
}

/**
 * @function see_joint_limits
 */
bool SpaceCharacterization::see_joint_limits( space_characterization::SeeJointLimits::Request &_req,
					      space_characterization::SeeJointLimits::Response &_res ) {

  // Get the pose of the EE
  KDL::JntArray q( limb_.num_dofs );
  for( int i = 0; i < joint_state_.position.size(); ++i ) {
    q(i) = joint_state_.position[i];
  }
  
  KDL::Frame Tf_ee;
  limb_.fk_solver->JntToCart( q, Tf_ee );

  // Get the joint solutions for this Cartesian pose
  KDL::Twist bounds=KDL::Twist::Zero();
  std::vector<KDL::JntArray> raw_goal_configs, goal_configs;
  limb_.joint_group->chain_solver->CartToJnt( q, Tf_ee,
					      raw_goal_configs, bounds );
  for( auto conf : raw_goal_configs  ) {
    // silent=true
    if( !limb_.joint_group->chain_solver->ChainCollisions( conf, true ) ) {
      goal_configs.push_back( conf );
    }
  }
    
  printf("# of Solutions: %lu \n", goal_configs.size() );

  
  // Get the limits for max-min
  std::vector<double> min_val(limb_.num_dofs);
  std::vector<double> max_val(limb_.num_dofs);
  for(int i = 0; i < limb_.num_dofs; ++i ) {
    min_val[i] = limb_.ub.data(i);
    max_val[i] = limb_.lb.data(i);
  }

  for( int i = 0; i < goal_configs.size(); ++i ) {
    for( int j = 0; j < limb_.num_dofs; ++j ) {
      if( goal_configs[i].data(j) < min_val[j] ) {
	min_val[j] = goal_configs[i].data(j);
      }
      if( goal_configs[i].data(j) > max_val[j] ) {
	max_val[j] = goal_configs[i].data(j);
      }
    }
  }


  for( int i=0; i < limb_.num_dofs; ++i ) {
    printf("J[%d] < %f -- %f > \n", i,
	   min_val[i]*180.0/3.1416,
	   max_val[i]*180.0/3.1416 );
  }
  
  return true;
}
/**
 * @function joint_state_cb
 */
void SpaceCharacterization::joint_state_cb( const sensor_msgs::JointState::ConstPtr &_msg ) {
  // Get ellipsoid (JJt)^-1
  if( _msg->position.size() != limb_.num_dofs ) { return; }
  printf("Got something! \n");
  KDL::JntArray q( limb_.num_dofs );
  for( int i = 0; i < _msg->position.size(); ++i ) {
    q(i) = _msg->position[i];
  }
  joint_state_ = *_msg;
  
  Eigen::MatrixXd Jlin, Jang, Jlin_inv, Jang_inv;

  limb_.getJacs( q,
		 Jlin, Jang,
		 Jlin_inv, Jang_inv );
  Eigen::MatrixXd m1; m1 = Jlin*Jlin.transpose();
  Eigen::MatrixXd m; m = m1.inverse();
  Eigen::EigenSolver<Eigen::MatrixXd> eig( m );
  Eigen::MatrixXd eigv; eigv = eig.eigenvectors().real();
  std::cout << "Eigenvectors: \n" << eigv << std::endl;
  Eigen::MatrixXd eigval; eigval = eig.eigenvalues().real();

  // Publish an ellipsoid marker with the ellipsoid drawn
  if( eigv.cols() != 3 || eigv.rows() != 3 || eigval.rows() != 3|| eigval.cols() < 1 ) {
    ROS_WARN("Eigenvectors should be 3x3, not %dx%d.!! Exiting ",
	     eigv.rows(), eigv.cols() );
    return;
  }

  KDL::Frame Tf_ee;
  limb_.fk_solver->JntToCart( q, Tf_ee );

  // Rot X axis
  Eigen::Quaterniond qi;
  Eigen::Vector3d xi(1,0,0);
  Eigen::VectorXd ni; ni = eigv.col(0); ni.normalize();
  qi = Eigen::Quaterniond::FromTwoVectors( xi, ni );
  
  // Make ellipsoid of max size 20 cm
  eigval.normalize();
  eigval = eigval*0.2;
  
  visualization_msgs::Marker msg;
  
  msg.header.stamp = ros::Time::now();
  msg.header.frame_id = limb_.base_link;
  msg.type = visualization_msgs::Marker::SPHERE;
  msg.action = 0; // ADD/MODIFY
  msg.scale.x = eigval(0); msg.scale.y = eigval(1); msg.scale.z = eigval(2);

  msg.pose.position.x = Tf_ee.p.x();
  msg.pose.position.y = Tf_ee.p.y();
  msg.pose.position.z = Tf_ee.p.z();
  double qox, qoy, qoz, qow;
  Tf_ee.M.GetQuaternion( qox, qoy, qoz, qow );
  Eigen::Quaterniond qo( qow, qox, qoy, qoz );
  Eigen::Quaterniond qf; qf = qo*qi;
  printf("Qo: %f %f %f %f \n", qox, qoy, qoz, qow );
  printf("Qi: %f %f %f %f \n", qi.x(), qi.y(), qi.z(), qi.w() );
  printf("Qf: %f %f %f %f \n", qf.x(), qf.y(), qf.z(), qf.w() );
  qf.normalize();
  msg.pose.orientation.x = qf.x();
  msg.pose.orientation.y = qf.y();
  msg.pose.orientation.z = qf.z();
  msg.pose.orientation.w = qf.w();
  
  msg.color.r = 1.0; msg.color.g = 0.0; msg.color.b = 1.0; msg.color.a = 1.0;

  pub_ellipsoid_marker_.publish( msg );
  printf("XYZ: %f %f %f \n", msg.pose.position.x, msg.pose.position.y, msg.pose.position.z );
}


/****/
bool SpaceCharacterization::getTf( std::string _source,
				   std::string _target,
				   tf::Transform &_Tf ) {

  tf::StampedTransform Tf_out;
  bool b;
  b =  listener_.waitForTransform( _source,
	  		           _target,
			           ros::Time(), ros::Duration(3.0) ); 
  listener_.lookupTransform( _source,
			     _target,
			     ros::Time(), Tf_out );
  
  _Tf = Tf_out;
  return b;
}


/**
 * @function spin
 */
void SpaceCharacterization::spin() {

}


/**
 * @
 */
void SpaceCharacterization::generate_samples( int _samples ) {

  ROS_WARN( "Generating workspace with %d samples....", _samples );

  // Reset samples, just in case
  samples_.clear();
  
  KDL::Frame Tf_ee;
  KDL::JntArray q( limb_.num_dofs );
  limb_.joint_group->chain_solver->PerformCollisionDetection(true);

  for( int i = 0; i < _samples; ++i ) {

    if( i % 10000 == 0 ) { printf("Loop %d/%d \n", i, _samples ); }
    
    q = limb_.generate_random_config( 1.0 );
    limb_.fk_solver->JntToCart( q, Tf_ee );
    
    // Create sample
    Sample_t st;
    st.q = q.data;
    st.p = Eigen::Vector3d( Tf_ee.p.x(), Tf_ee.p.y(), Tf_ee.p.z() );
    st.cluster = -1;

    // Store sample
    samples_.push_back( st );
  }

  
}

void SpaceCharacterization::generate_clusters( size_t _clusters ) {

  clusters_ = _clusters;

  // Load
  arma::mat ps = arma::mat(3, samples_.size() );

  std::vector<Sample_t>::iterator it;
  int idx = 0;
  for( auto it : samples_ ) {
    ps(0,idx) = it.q(0);
    ps(1,idx) = it.q(1);
    ps(2,idx) = it.q(2);
    idx++;
    
  }

  arma::Row<size_t> assignments;
  arma::mat centroids;
  
  mlpack::kmeans::KMeans<> km;
  km.Cluster( ps, clusters_, assignments, centroids );

  // Store assignment
  if( assignments.n_elem != samples_.size() ) {
    ROS_WARN("Assignment size different than samples size!!!");
    return;
  }

  int counter = 0;
  for( auto &it : samples_ ) {
    it.cluster = assignments(counter);
    counter++;
  }

}

/**
 * @function store
 */
bool SpaceCharacterization::store() {

  std::string pkg_path = ros::package::getPath("space_characterization");

  ROS_WARN("Storing workspace....");
  
  for( int i = 0; i < clusters_; ++i ) {

    std::string filename = pkg_path + std::string("/robot_clusters_") + std::to_string(i) + std::string(".txt");
    std::ofstream output( filename.c_str(), std::ofstream::out );

    for( auto it : samples_ ) {
      if( it.cluster == i ) { output << it ; }
      //output << it;
    }
    output << " " << std::endl;
    output << " " << std::endl;

    output.close();  


  }
  
  ROS_INFO("Finished storing  %lu points",  samples_.size() );

  
}
