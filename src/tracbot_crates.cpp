#include <ros/ros.h>
#include <robot_interaction_tools_msgs/AttachCollisionObject.h>
#include <robot_interaction_tools_msgs/DeattachCollisionObject.h>

/***/
int main( int argc, char* argv[] ) {

  ros::init( argc, argv, "attach_deattach" );
  ros::NodeHandle nh;
  ros::Rate r(15.0);

    ros::ServiceClient client_attach = nh.serviceClient<robot_interaction_tools_msgs::AttachCollisionObject>("/planner_node/attach_collision_object");
  ros::ServiceClient client_deattach = nh.serviceClient<robot_interaction_tools_msgs::DeattachCollisionObject>("/planner_node/deattach_collision_object");

  robot_interaction_tools_msgs::AttachCollisionObject srv_attach;
  robot_interaction_tools_msgs::DeattachCollisionObject srv_deattach;


  r.sleep(); ros::spinOnce();
  
  // 1. Attach
  ROS_WARN("Attaching crate 1...");
   
  robot_interaction_tools_msgs::CollisionObjectInfo coll_a;
  
  coll_a.type = "mesh";
  coll_a.mesh = "package://space_characterization/meshes/tall_crate.stl";
  coll_a.scale.x = 1.0;
  coll_a.scale.y = 1.0;
  coll_a.scale.z = 1.0;

  coll_a.offset.pose.position.x = 0.8;
  coll_a.offset.pose.position.y = -0.3;
  coll_a.offset.pose.position.z = 0.0;
  coll_a.offset.pose.orientation.x = 0.0;
  coll_a.offset.pose.orientation.y = 0.0;
  coll_a.offset.pose.orientation.z = 0.0;
  coll_a.offset.pose.orientation.w = 1.0;
  coll_a.name = "crate_1";
  coll_a.offset.header.stamp = ros::Time::now();
  coll_a.offset.header.frame_id = "base_footprint"; //"tracbot/left_hand_stationary_link";

  srv_attach.request.collision_object = coll_a;
  
  ROS_WARN("test_attach: Srv attach sent with name: %s and parent: %s ",
	   srv_attach.request.collision_object.name.c_str(),
	   srv_attach.request.collision_object.offset.header.frame_id.c_str() );
  if (client_attach.call(srv_attach)) {
    ROS_INFO("Good, link %s was attached",
	     srv_attach.request.collision_object.name.c_str() );
  } else {
    ROS_ERROR("No link was attached");
  }

  // Add crate_2
  coll_a.name = "crate_2";
  coll_a.offset.pose.position.x = 0;
  coll_a.offset.pose.position.y = -0.6;
  srv_attach.request.collision_object = coll_a;
  
  ROS_WARN("test_attach: Srv attach sent with name: %s and parent: %s ",
	   srv_attach.request.collision_object.name.c_str(),
	   srv_attach.request.collision_object.offset.header.frame_id.c_str() );
  if (client_attach.call(srv_attach)) {
    ROS_INFO("Good, link %s was attached",
	     srv_attach.request.collision_object.name.c_str() );
  } else {
    ROS_ERROR("No link was attached");
  }

  
  ROS_WARN("Sleeping and spinning");
  r.sleep(); ros::spinOnce();
  usleep(5.0*1e6);
  
  // 6. Go on
  return 0;
}
