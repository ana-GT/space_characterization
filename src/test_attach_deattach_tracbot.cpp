#include <ros/ros.h>
#include <robot_interaction_tools_msgs/AttachCollisionObject.h>
#include <robot_interaction_tools_msgs/DeattachCollisionObject.h>

/***/
int main( int argc, char* argv[] ) {

  ros::init( argc, argv, "attach_deattach" );
  ros::NodeHandle nh;
  ros::Rate r(15.0);

    ros::ServiceClient client_attach = nh.serviceClient<robot_interaction_tools_msgs::AttachCollisionObject>("/planner_node/attach_collision_object");
  ros::ServiceClient client_deattach = nh.serviceClient<robot_interaction_tools_msgs::DeattachCollisionObject>("/planner_node/deattach_collision_object");

  robot_interaction_tools_msgs::AttachCollisionObject srv_attach;
  robot_interaction_tools_msgs::DeattachCollisionObject srv_deattach;


  r.sleep(); ros::spinOnce();
  
  // 1. Attach
  ROS_WARN("Attaching...");
   
  robot_interaction_tools_msgs::CollisionObjectInfo coll_a;
  
  coll_a.type = "mesh";
  //coll_a.mesh = "package://tracbot_description/meshes/test_cube.stl";
  coll_a.mesh = "package://space_characterization/meshes/kiva_pod.stl";
  coll_a.scale.x = 1.0;
  coll_a.scale.y = 1.0;
  coll_a.scale.z = 1.0;
  /*
  coll_a.type = "cylinder";
  coll_a.scale.x = 0.05;
  coll_a.scale.y = 0.12;
  coll_a.scale.z = 0.0;*/
  /*
  coll_a.type = "box";
  coll_a.scale.x = 0.1;
  coll_a.scale.y = 0.1;
  coll_a.scale.z = 0.1;
  */
  coll_a.offset.pose.position.x = 1.1;
  coll_a.offset.pose.position.y = 0.0;
  coll_a.offset.pose.position.z = 0.0;
  coll_a.offset.pose.orientation.x = 0.0;
  coll_a.offset.pose.orientation.y = 0.0;
  coll_a.offset.pose.orientation.z = 0.0;
  coll_a.offset.pose.orientation.w = 1.0;
  coll_a.name = "kiva_pod";
  coll_a.offset.header.stamp = ros::Time::now();
  coll_a.offset.header.frame_id = "base_footprint"; //"tracbot/left_hand_stationary_link";

  srv_attach.request.collision_object = coll_a;
  
  ROS_WARN("test_attach: Srv attach sent with name: %s and parent: %s ",
	   srv_attach.request.collision_object.name.c_str(),
	   srv_attach.request.collision_object.offset.header.frame_id.c_str() );
  if (client_attach.call(srv_attach)) {
    ROS_INFO("Good, link %s was attached",
	     srv_attach.request.collision_object.name.c_str() );
  } else {
    ROS_ERROR("No link was attached");
  }

  ROS_WARN("Sleeping and spinning");
  r.sleep(); ros::spinOnce();
  usleep(5.0*1e6);
/*
  // 2. Deattach
  srv_deattach.request.attach_link_name = "kiva_pod";
  
  if (client_deattach.call(srv_deattach)) {
    ROS_INFO("Good, link %s was deattached",
	     srv_deattach.request.attach_link_name.c_str() );
  } else {
    ROS_ERROR("No link was deattached");
  }

  ROS_WARN("Sleeping and spinning");
  r.sleep(); ros::spinOnce();
  usleep(5.0*1e6);

  // 3. Attach again
  coll_a.type = "cylinder";
  coll_a.scale.x = 0.05;
  coll_a.scale.y = 0.12;
  coll_a.scale.z = 0.0;
  coll_a.offset.header.stamp = ros::Time::now();
  srv_attach.request.collision_object = coll_a;

  if (client_attach.call(srv_attach)) {
    ROS_INFO("Good, link %s was re-attached",
	     srv_attach.request.collision_object.name.c_str() );
  } else {
    ROS_ERROR("No link was re-attached");
  }

  ROS_WARN("Sleeping and spinning again");
  r.sleep(); ros::spinOnce();
  usleep(5.0*1e6);

  
  // 4. Deattach
  if (client_deattach.call(srv_deattach)) {
    ROS_INFO("Good, link %s was re-deattached",
	     srv_deattach.request.attach_link_name.c_str() );
  } else {
    ROS_ERROR("No link was re-deattached");
  }

  ROS_WARN("Sleeping and spinning");
  r.sleep(); ros::spinOnce();
  usleep(5.0*1e6);
 
  // 5. Attach again
  coll_a.type = "sphere";
  coll_a.scale.x = 0.10;
  coll_a.scale.y = 0.0;
  coll_a.scale.z = 0.0;
  coll_a.offset.header.stamp = ros::Time::now();
  srv_attach.request.collision_object = coll_a;

  if (client_attach.call(srv_attach)) {
    ROS_INFO("Good, link %s was re-attached",
	     srv_attach.request.collision_object.name.c_str() );
  } else {
    ROS_ERROR("No link was re-attached");
  }

  ROS_WARN("Sleeping and spinning again");
  r.sleep(); ros::spinOnce();
  usleep(5.0*1e6);
 */
  
  // 6. Go on
  return 0;
}
