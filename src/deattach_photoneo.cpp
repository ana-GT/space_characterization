#include <ros/ros.h>
#include <robot_interaction_tools_msgs/AttachCollisionObject.h>
#include <robot_interaction_tools_msgs/DeattachCollisionObject.h>

/***/
int main( int argc, char* argv[] ) {

  ros::init( argc, argv, "deattach_photoneo" );
  ros::NodeHandle nh;
  ros::Rate r(15.0);

    ros::ServiceClient client_attach = nh.serviceClient<robot_interaction_tools_msgs::AttachCollisionObject>("/planner_node/attach_collision_object");
  ros::ServiceClient client_deattach = nh.serviceClient<robot_interaction_tools_msgs::DeattachCollisionObject>("/planner_node/deattach_collision_object");

  robot_interaction_tools_msgs::AttachCollisionObject srv_attach;
  robot_interaction_tools_msgs::DeattachCollisionObject srv_deattach;


  r.sleep(); ros::spinOnce();
  

  // 2. Deattach
  srv_deattach.request.attach_link_name = "photoneo_mount";
  
  if (client_deattach.call(srv_deattach)) {
    ROS_INFO("Good, link %s was deattached",
	     srv_deattach.request.attach_link_name.c_str() );
  } else {
    ROS_ERROR("No link was deattached");
  }

  ROS_WARN("Sleeping and spinning");
  r.sleep(); ros::spinOnce();
  usleep(0.1*1e6);

  // 6. Go on
  return 0;
}
