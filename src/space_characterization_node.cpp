#include <ros/ros.h>
#include <space_characterization/space_characterization.h>

/**
 * @function main
 */
int main( int argc, char* argv[] ) {

  ros::init( argc, argv, "space_characterization_node" );
  ros::NodeHandle nh;

  ROS_WARN("Generate space characterization");
  SpaceCharacterization sc( nh );
  ROS_WARN("Init");
  std::string group = "left_arm";
  std::string ee_link = "tracbot/left_hand_fingertips";// "tool1"; // tracarm/hand_fingertips
  sc.init( group, ee_link );
  /*ROS_WARN("Generate samples");
  int samples = 100000;
  sc.generate_samples( samples );
  int N = 7;
  ROS_WARN("Generate clusters");
  sc.generate_clusters(N);
  ROS_WARN("Store info" );
  sc.store();*/
  ROS_WARN("Now just spin....");
  ros::Rate r(20.0);
  while( ros::ok() ) {
    ros::spinOnce();
    sc.spin();	
    r.sleep();
  }
  
}
