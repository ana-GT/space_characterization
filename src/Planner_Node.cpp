/**
 * @file Planner_Node.cpp
 */
#include <space_characterization/Planner_Node.h>

/**
 * @function Planner_Node
 * @brief Constructor
 */
Planner_Node::Planner_Node( ros::NodeHandle _nh ) :
  nh_(_nh),
  server_("planner_node_server") {
  
}

/**
 * @function set_robot_params
 */
bool Planner_Node::set_robot_params() {

  std::string srdf( "/robot_description_semantic" );
  std::string urdf( "/robot_description" );

  nh_.getParam( "robot_description_file", urdf );
  ROS_WARN("Robot Description parameter used: %s", urdf.c_str() );

  nh_.getParam("robot_description_semantic_file", srdf );
  ROS_WARN("Robot Description Semantic: %s", srdf.c_str() );

  // Get rdf
  rdf_model_.reset( new rit_utils::RDFModel() );
  if( !rdf_model_->init( srdf, urdf ) ) {
    ROS_ERROR("Failure building RDF Model");
  }

  group_ = std::string("arm");
  nh_.getParam("group", group_ );
  ROS_WARN("Group: %s", group_.c_str() );
  
  // Get Robot specifics
  // Get joint group from group name
  if( !rdf_model_->getJointGroup( group_, joint_group_ ) ) {
    ROS_ERROR("Failure getting joint group for %s", group_.c_str() );
    return false;
  }

  // Solver
  solver_.reset( new TRAC_IK::CartesianTrajectories(joint_group_) );

  
}

/**
 * @function create_3D_marker
 */
visualization_msgs::InteractiveMarker Planner_Node::create_3D_marker( const robot_interaction_tools_msgs::CollisionObjectInfo &_msg ) {

  visualization_msgs::InteractiveMarker im;
  im.header.frame_id = _msg.offset.header.frame_id;
  im.header.stamp=ros::Time::now();
  im.name = _msg.name;
  im.description = "Simple 3-DOF Control";
  im.pose = _msg.offset.pose;
  
  // create a grey box marker
  visualization_msgs::Marker marker;
  if( _msg.type == std::string("box") ) {
    marker.type = visualization_msgs::Marker::CUBE;
  } else if( _msg.type == std::string("cylinder") ) {
    marker.type = visualization_msgs::Marker::CYLINDER;
  } else if( _msg.type == std::string("sphere") ) {
    marker.type = visualization_msgs::Marker::SPHERE;
  } else {
    ROS_ERROR("By now only accepting primitive shapes!");
    return im;
  }

  marker.scale = _msg.scale;
  marker.color.r = 0.5; marker.color.g = 0.5;
  marker.color.b = 0.5; marker.color.a = 1.0;

  // create a non-interactive control which contains the box
  visualization_msgs::InteractiveMarkerControl viz_control;
  viz_control.always_visible = true;
  viz_control.markers.push_back( marker );
  viz_control.interaction_mode = visualization_msgs::InteractiveMarkerControl::MOVE_ROTATE_3D;
  // add the control to the interactive marker
  im.controls.push_back( viz_control );

  // create a control which will move the box
  // this control does not contain any markers,
  // which will cause RViz to insert two arrows
  visualization_msgs::InteractiveMarkerControl move_control;

  tf::Quaternion orien(1.0, 0.0, 0.0, 1.0);
  orien.normalize();
  tf::quaternionTFToMsg(orien, move_control.orientation);
  move_control.name = "move_x";
  move_control.interaction_mode = visualization_msgs::InteractiveMarkerControl::MOVE_AXIS;
  im.controls.push_back(move_control);

  orien = tf::Quaternion(0.0, 1.0, 0.0, 1.0 );
  orien.normalize();
  tf::quaternionTFToMsg(orien, move_control.orientation);
  move_control.name = "rotate_z";
  move_control.interaction_mode = visualization_msgs::InteractiveMarkerControl::ROTATE_AXIS;
  im.controls.push_back(move_control);
  move_control.name = "move_z";
  move_control.interaction_mode = visualization_msgs::InteractiveMarkerControl::MOVE_AXIS;
  im.controls.push_back(move_control);

  orien = tf::Quaternion(0.0, 0.0, 1.0, 1.0 );
  orien.normalize();
  tf::quaternionTFToMsg(orien, move_control.orientation);
  move_control.name = "move_y";
  move_control.interaction_mode = visualization_msgs::InteractiveMarkerControl::MOVE_AXIS;
  im.controls.push_back(move_control);

  return im;
}

/**
 * @function add_interactive_marker
 */
void Planner_Node::add_interactive_marker( visualization_msgs::InteractiveMarker _im ) {

  server_.insert( _im, boost::bind(&Planner_Node::process_feedback, this,_1) );
  server_.applyChanges();
}

/**
 * @function process_feedback
 */
void Planner_Node::process_feedback( const visualization_msgs::InteractiveMarkerFeedbackConstPtr &_feedback ) {
  process_feedback_( _feedback );
}


/**
 * @function process_feedback
 */
void Planner_Node::process_feedback_( const visualization_msgs::InteractiveMarkerFeedbackConstPtr &_feedback ) {

}

