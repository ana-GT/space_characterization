/**
 * @file test_update_urdf_description.cpp
 * @brief
 */
#include <ros/ros.h>
#include <tf/transform_broadcaster.h>
#include <tinyxml2.h>
#include <robot_interaction_tools_msgs/CollisionObjectInfo.h>
#include <mutex>

/*********************/
class UpdateUrdf {
  
public:
  UpdateUrdf( ros::NodeHandle _nh ) :
    nh_(_nh) {
    
  }

  /****/
  bool init() {
    ROS_WARN("PublisherCollisionUrdf: Init");
    if( !upload_urdf_description() ) { return false; }

    // Subscribe to topic
    sub_attach_urdf_ = nh_.subscribe( "test_attach_collision_object",
				      1, &UpdateUrdf::attach_collision_object_cb, this );
    sub_deattach_urdf_ = nh_.subscribe( "test_deattach_collision_object",
				      1, &UpdateUrdf::deattach_collision_object_cb, this );
    
  }

  /*****/
  bool upload_urdf_description() {
    ROS_WARN("PublisherCollisionUrdf: Upload urdf description");
    // Load urdf parameter
    std::string robot_description_string;
    if( !nh_.getParam( "robot_description", robot_description_string ) ) {
      ROS_ERROR("No robot_description parameter found, exiting!");
      return false;
    }
    
    // Parse into a tinyXML2 document
    doc_.Parse( robot_description_string.c_str() );
    if( !doc_.RootElement() ) {
      ROS_WARN("URDF failed XML Parse (not root!!)");
      return false;
    }
    
    return true;      
  }

  /****/
  void deattach_collision_object_cb(const robot_interaction_tools_msgs::CollisionObjectInfo::ConstPtr& msg) {

    ROS_WARN("Delete collision object cb");
    // Delete something to it
    tinyxml2::XMLElement* root = doc_.RootElement();

    for( tinyxml2::XMLElement* child = root->FirstChildElement(); child !=0; child = child->NextSiblingElement() ) {
      if( child != NULL ) {
	std::string name_str = child->Name();
        if( name_str == std::string("joint") || name_str == std::string("link") ) {
          const char* ni = child->Attribute( "name" );
          std::string ni_str = ni;
          if( ni_str == msg->name ) {
            ROS_WARN("DEBUG: Name: %s . Deleting this %s \n", ni_str.c_str(), name_str.c_str() );
	    //root->DeleteChild( child );
            doc_.DeleteNode( child );
          }
        }  
      }
    }

    // Delete attach parent link    
    mux_.lock();
    for( std::vector<std::string>::iterator it = attached_links_.begin(); it != attached_links_.end(); ++it ) {
      if( *it == msg->name ) {
        attached_links_.erase( it ); break;
      }
    }
    parent_links_.erase( msg->name );
    mux_.unlock();

    // Publish updated urdf
    publish_updated_urdf();
    
  }
  
  /****/
  void attach_collision_object_cb(const robot_interaction_tools_msgs::CollisionObjectInfo::ConstPtr& msg) {

    ROS_WARN("Add collision object cb");
    // Add something to it
    tinyxml2::XMLElement* root = doc_.RootElement();
    
    tinyxml2::XMLElement* attached_link = doc_.NewElement("link");
    attached_link->SetAttribute( "name", msg->name.c_str() );

    std::string parent_link, child_link;
    double x,y,z, roll, pitch, yaw;
    double dimx, dimy, dimz;
    
    parent_link = msg->offset.header.frame_id;
    child_link = msg->name;
    
    dimx = msg->scale.x;
    dimy = msg->scale.y;
    dimz = msg->scale.z;
    
    x = msg->offset.pose.position.x;
    y = msg->offset.pose.position.y;
    z = msg->offset.pose.position.z;
    
    tf::Matrix3x3 rot( tf::Quaternion( msg->offset.pose.orientation.x,
				       msg->offset.pose.orientation.y,
				       msg->offset.pose.orientation.z,
				       msg->offset.pose.orientation.w ) );
    rot.getRPY( roll, pitch, yaw );
    
    tinyxml2::XMLElement* visual = doc_.NewElement("visual");
    fill_viz_elem( visual,
		   msg->type,
		   msg->mesh,
		   dimx, dimy, dimz,
		   x,y,z, roll, pitch, yaw ); 
  
    tinyxml2::XMLElement* collision = doc_.NewElement("collision");
    fill_viz_elem( collision,
		   msg->type,
		   msg->mesh,
		   dimx, dimy, dimz,
		   x, y, z, roll, pitch, yaw );

    attached_link->InsertEndChild( visual );
    attached_link->InsertEndChild(collision);
    root->InsertEndChild( attached_link );

    tinyxml2::XMLElement* attached_joint = doc_.NewElement("joint");
    fill_joint_elem( attached_joint,
		     msg->name.c_str(),
		     parent_link,
		     child_link );
    root->InsertEndChild( attached_joint );

    // Store attached transform and parent link    
    mux_.lock();
    parent_links_[child_link] = parent_link;

    bool link_already_there = false;
    for( int i = 0; i < attached_links_.size(); ++i ) {
      if( attached_links_[i] == child_link ) { link_already_there = true; break; }
    }
    if( !link_already_there ) { attached_links_.push_back( child_link ); }
    mux_.unlock();

    // Publish updated urdf
    publish_updated_urdf();
  }

  /****/
  void publish_updated_urdf() {
    // Save it to a parameter (tracik_urdf_description maybe?)
    tinyxml2::XMLPrinter printer;

    doc_.Print( &printer );
    std::string trac_ik_robot_description_string = printer.CStr();
    nh_.setParam( "/trac_ik_robot_description", trac_ik_robot_description_string );    
  }
  
  /****/
  void fill_viz_elem( tinyxml2::XMLElement* _viz,
		      std::string _type,
		      std::string _mesh_filename,
		      double _dimx, double _dimy, double _dimz,
		      double _x, double _y, double _z,
		      double _roll, double _pitch, double _yaw ) {

    tinyxml2::XMLElement* origin =  doc_.NewElement("origin");
    std::string xyz = std::to_string(_x) + " " + std::to_string(_y) + " " + std::to_string(_z);
    std::string rpy = std::to_string(_roll) + " " + std::to_string(_pitch) + " " + std::to_string(_yaw);
    origin->SetAttribute("xyz", xyz.c_str() );
    origin->SetAttribute("rpy", rpy.c_str() );
    _viz->InsertEndChild( origin );

    tinyxml2::XMLElement* geometry = doc_.NewElement("geometry");

    if( _type == "cylinder" ) {

      tinyxml2::XMLElement* geometry_prim = doc_.NewElement("cylinder");
      geometry_prim->SetAttribute("radius", _dimx );
      geometry_prim->SetAttribute("length", _dimy );
      geometry->InsertEndChild( geometry_prim );      
    } else if( _type == "box" ) {

      std::string size_dim = std::to_string(_dimx) + " " + std::to_string(_dimy) + " " + std::to_string(_dimz);
      tinyxml2::XMLElement* geometry_prim = doc_.NewElement("box");
      geometry_prim->SetAttribute("size", size_dim.c_str() );
      geometry->InsertEndChild( geometry_prim );      
    } else if( _type == "sphere" ) {

      tinyxml2::XMLElement* geometry_prim = doc_.NewElement("sphere");
      geometry_prim->SetAttribute("radius", _dimx );
      geometry->InsertEndChild( geometry_prim );      
    } else if( _type == "mesh" ) {

      tinyxml2::XMLElement* geometry_prim = doc_.NewElement("mesh");
      geometry_prim->SetAttribute("filename", _mesh_filename.c_str() );
      geometry->InsertEndChild( geometry_prim );      
    }
    _viz->InsertEndChild( geometry );
    
  }

  /****/
  void loop() {

    tf::Transform transform;
    transform.setOrigin( tf::Vector3(0,0,0) );
    tf::Quaternion q;
    q.setRPY( 0, 0, 0 );
    transform.setRotation( q );

    mux_.lock();
    for( int i = 0; i < attached_links_.size(); ++i ) {

      br_.sendTransform( tf::StampedTransform( transform,
					       ros::Time::now(),
					       parent_links_[  attached_links_[i] ],
					       attached_links_[i] ) );
      
    } // end for
    mux_.unlock();

  }

  /***/
  void fill_joint_elem( tinyxml2::XMLElement* _joint,
			std::string _joint_name,
			std::string _parent_link,
			std::string _child_link ) {

    _joint->SetAttribute("name", _joint_name.c_str() );
    _joint->SetAttribute("type", "fixed");

    double _x = 0; double _y = 0; double _z = 0;
    double _roll = 0; double _pitch = 0; double _yaw = 0;
    tinyxml2::XMLElement* origin =  doc_.NewElement("origin");
    std::string xyz = std::to_string(_x) + " " + std::to_string(_y) + " " + std::to_string(_z);
    std::string rpy = std::to_string(_roll) + " " + std::to_string(_pitch) + " " + std::to_string(_yaw);
    origin->SetAttribute("xyz", xyz.c_str() );
    origin->SetAttribute("rpy", rpy.c_str() );
    _joint->InsertEndChild( origin );
    
    tinyxml2::XMLElement* parent = doc_.NewElement("parent");
    parent->SetAttribute("link", _parent_link.c_str() );
    _joint->InsertEndChild( parent );
    
    tinyxml2::XMLElement* child = doc_.NewElement("child");
    child->SetAttribute("link", _child_link.c_str() );
    _joint->InsertEndChild( child );
  
  }
  
protected:
  ros::Subscriber sub_attach_urdf_;
  ros::Subscriber sub_deattach_urdf_;
  
  ros::NodeHandle nh_;
  tinyxml2::XMLDocument doc_;

  //
  tf::TransformBroadcaster br_;
  std::map<std::string, std::string> parent_links_;
  std::vector<std::string> attached_links_; 
  std::mutex mux_; 
};

/****************************************/
int main( int argc, char* argv[] ) {

  ros::init( argc, argv, "publisher_collision_urdf" );
  ros::NodeHandle nh;

  ROS_WARN("************* Test update urdf description *************");
  UpdateUrdf uu( nh );
  uu.init();
  
  ros::Rate r(30.0);
  while( ros::ok() ) {
    ros::spinOnce();
    uu.loop();
    r.sleep();
  }
 
  
  return 0;
}
