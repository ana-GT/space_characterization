/**
 * @file Pick.cpp
 */

#include <space_characterization/Pick.h>

/**
 * @function Pick
 * @brief Constructor
 */
Pick::Pick() {

}

/**
 * @function set_arm_start
 */
void Pick::set_arm_start_configuration( const sensor_msgs::JointState &_qs ) {
  qs_ = _qs;
}

/**
 * @function set_object
 */
void Pick::set_object( const robot_interaction_tools_msgs::CollisionObjectInfo &_object ) {
  object_ = _object;
}

/**
 * @function set_Tobj
 */
void Pick::set_Tobj( const geometry_msgs::Pose &_Tobj ) {
  Tobj_ = _Tobj;
}
