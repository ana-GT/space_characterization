
#include <space_characterization/Pick_Planner.h>

/**
 * @function Pick_Planner
 */
Pick_Planner::Pick_Planner( ros::NodeHandle _nh,
			    std::shared_ptr<TRAC_IK::JointGroup> _joint_group,
			    std::shared_ptr<TRAC_IK::CartesianTrajectories> _solver,
			    std::string _robot_name ) {
  nh_ = _nh;
  joint_group_ = _joint_group;
  solver_ = _solver;

  std::string traj_name = std::string("/") + _robot_name +
    std::string("/joint_state_controller/joint_command");
  traj_pub_ = nh_.advertise<sensor_msgs::JointState>( traj_name.c_str(), 1 );
}

bool Pick_Planner::setConditions( Pick &_pick ) {

}

int Pick_Planner::calculateGrasps( Pick &_pick,
				   Eigen::VectorXd _data,
				   int _grasp_prim_type,
				   double _pregrasp_dist ) {
  return 0;
}

bool Pick_Planner::calculate_path( Pick &_pick,
				   double _app_dist,
				   double _up_dist ) {

  plan_available_ = false;
  KDL::JntArray qs;
  KDL::Frame pg;
  trajectory_msgs::JointTrajectory traj;
  KDL::Twist bounds = KDL::Twist::Zero();
  
  solver_->SolveTraj( qs, pg, traj, bounds ); 
  if( traj.points.size() > 3 ) {

    plan_available_ = true;
  }
  
  return plan_available_;
}

/**
 * @function execute_path
 */
void Pick_Planner::execute_path() {

  if( !plan_available_ ) {
    ROS_ERROR("There is not plan available, not executing anything!");
    return;
  }

  ros::Rate r(10.0);
  for( auto pi : path_ ) {
    traj_pub_.publish( pi );
    r.sleep();
  }
  
}
