/**
 * @file pick_planner_node.cpp
 */
#include <space_characterization/pick_planner_node.h>

/**
 * @function Pick_Planner_Node
 * @brief Constructor
 */
Pick_Planner_Node::Pick_Planner_Node( ros::NodeHandle &_nh ) :
  Planner_Node(_nh) {
  
  // Set menu handler
  menu_handler_.insert("Plan Pick",
		       boost::bind(&Pick_Planner_Node::plan_pick, this,_1) );
  menu_handler_.insert("Execute Pick",
		       boost::bind(&Pick_Planner_Node::execute_pick, this,_1) );

  
}

/**
 * @function plan_pick
 */
void Pick_Planner_Node::plan_pick( const visualization_msgs::InteractiveMarkerFeedbackConstPtr &_feedback ) {

  //********** Set pick problem ***************

  // 1. Get current arm pose
  sensor_msgs::JointState qs;
  if( !rdf_model_->getJointState( group_, qs ) ) {
    ROS_ERROR("Failure getting current joint state");
    return;
  }
  pick_.set_arm_start_configuration( qs );
  pick_.set_object( object_ );
  // Get current pose of the marker
  visualization_msgs::InteractiveMarker im;
  if( !server_.get( object_.name, im ) ) {
    ROS_WARN("Pick marker wasn't found");
    return;
  }
  pick_.set_Tobj( im.pose );

  //********** Call planner *******************
  double app_dist = 0.1;
  double up_dist = 0.1;
  pick_planner_->calculate_path( pick_, app_dist, up_dist );
}

/**
 * @function execute_pick
 */
void Pick_Planner_Node::execute_pick( const visualization_msgs::InteractiveMarkerFeedbackConstPtr &_feedback ) {

  // Call execute path
  pick_planner_->execute_path();
}

/**
 * @function set_plan_config
 */
void Pick_Planner_Node::set_plan_config() {

  // Add pick marker
  object_.name = "pick";
  object_.type = "cylinder";
  object_.scale.x = 0.1;
  object_.scale.y = 0.1;
  object_.scale.z = 0.4;
  object_.offset.header.frame_id = joint_group_->base_link;
  object_.offset.header.stamp = ros::Time::now();
  object_.offset.pose.position.x = 0.5;
  object_.offset.pose.position.y = 0.0;
  object_.offset.pose.position.z = 0.5;
  object_.offset.pose.orientation.w = 1.0;

  visualization_msgs::InteractiveMarker im_pick;
  im_pick = create_3D_marker( object_ );
  add_interactive_marker( im_pick ); 

  menu_handler_.apply( server_, im_pick.name );
  server_.applyChanges();

  // Set planner
  pick_planner_.reset( new Pick_Planner( nh_, joint_group_, solver_,
					 rdf_model_->getName() ) );
  
}

//////////////////////////////////////////////////////////////

/**
 * @function main
 */
int main( int argc, char* argv[] ) {

  ros::init( argc, argv, "pick_planner_node" );
  ros::NodeHandle nh;
  
  Pick_Planner_Node ppn( nh );
  // Set urdf/srdf/robot group used for manipulation
  ppn.set_robot_params();
  // Set markers to guide the plan parameters
  ppn.set_plan_config();

  ros::Rate r(30);
  while( ros::ok() ) {
    r.sleep();
    ros::spinOnce();
  }
}
